
from PIL import Image, ImageCms
import numpy as np
import scipy.fftpack
import scipy as sp
import cv2
from colorthief import ColorThief
from tempfile import TemporaryFile
import statistics 
from tempfile import TemporaryFile
import matplotlib.pyplot as plt
from scipy.spatial import distance

def fingerprint(path):
    im = Image.open(path).convert("L").resize((256,256), Image.BICUBIC)
    # im.show()
    grids = []
    grids_ravel = []
    i,j = 4,4
    while j<=28:
        i = 4
        while i<=28:
            box = (i*8, (j-1)*8, i*8 +8, j*8)
            # grids.append.asarray((im.crop(box)))
            ar = np.asarray(im.crop(box),dtype=np.int)
            
            # print(ar[0])
            grids.append(ar.tolist())
            grids_ravel.append(ar.ravel().tolist())
            i+=1
        j+=1

    mean = []
    for i in range(len(grids_ravel)):
        if statistics.mean(grids_ravel[i]) <52:
            # mean.append(51)
            mean.append(int(-2))
        elif statistics.mean(grids_ravel[i]) <103:
            # mean.append(102)
            mean.append(int(-1))
        elif statistics.mean(grids_ravel[i]) <154:
            # mean.append(153)
            mean.append(int(0))
        elif statistics.mean(grids_ravel[i]) <205:
            # mean.append(204)
            mean.append(int(1))
        elif statistics.mean(grids_ravel[i]) <256:
            # mean.append(255)
            mean.append(int(2))

    # print(len(mean))
    # print(mean)

    mean_as_array = np.asarray(mean).reshape((25,25))
    # plt.imshow(mean_as_array, cmap="gray")
    # plt.show()
    # print(mean_as_array)


    diff = list()

    for j in range(25):
        for i in range(25):
            diff.append([0,0,0,0,0,0,0,0])
            if(j-1)>= 0 and (i-1)>=0:
                diff[j][0] = int(mean_as_array[j][i] - mean_as_array[j-1][i-1])
            if(j-1)>=0:
                diff[j][1] = int(mean_as_array[j][i] - mean_as_array[j-1][i])
            if(j-1)>=0 and (i+1)<25:
                diff[j][2] = int(mean_as_array[j][i] - mean_as_array[j-1][i+1])
            if (i-1)>=0:
                diff[j][3] = int(mean_as_array[j][i] - mean_as_array[j][i-1])
            if (i+1)<25:
                diff[j][4] = int(mean_as_array[j][i] - mean_as_array[j][i+1])
            if (j+1)<25 and (i-1)>=0:
                diff[j][5] = int(mean_as_array[j][i] - mean_as_array[j+1][i-1])
            if (j+1)<25:
                diff[j][6] = int(mean_as_array[j][i] - mean_as_array[j+1][i])
            if (j+1)<25 and (i+1)<25:
                diff[j][7] = int(mean_as_array[j][i] - mean_as_array[j+1][i+1])


    return np.asarray(diff).ravel()

# print(fingerprint("/home/parsa/ProjectII/implementation/pier_sea_surf_121897_1920x1080.jpg"))
# np.savetxt('test.out', fingerprint("/home/parsa/ProjectII/implementation/pier_sea_surf_121897_1920x1080.jpg"),fmt='%d' ,delimiter=',',)
# X = np.random.random((100, 100)) # sample 2D array

fng_print1 = fingerprint(PATH/TO/FIRST/IMAGE)
fng_print2 = fingerprint(PATH/TO/SECOND/IMAGE)

# fng_print2 = fingerprint("/home/parsa/ProjectII/implementation/photo_2019-11-11_18-35-04.jpg")

diff = distance.hamming(fng_print1, fng_print2)
print(diff)