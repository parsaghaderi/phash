from PIL import Image, ImageCms
import numpy as np
import scipy.fftpack
import cv2
import pywt
from scipy.spatial import distance

def DCT(image, hash_size=16, highfreq_factor=4):
	img_size = 256
	image = image.convert("L").resize((img_size, img_size), Image.BICUBIC)

	srgb_profile = ImageCms.createProfile("sRGB")
	lab_profile  = ImageCms.createProfile("LAB")
	rgb2lab_transform = ImageCms.buildTransformFromOpenProfiles(srgb_profile, lab_profile, "RGB", "LAB")
	
	L = ImageCms.applyTransform(image,rgb2lab_transform )
	L_channel, a, b = L.split()

	grid = np.array(L_channel.getdata(), dtype=np.float).reshape((img_size, img_size))

	dct = scipy.fftpack.dct(scipy.fftpack.dct(grid, axis=0), axis=1)
	# print(dct)
	display = Image.fromarray(dct)
	display.show()

	dctlowfreq = dct[:hash_size, :hash_size]
	# print(dctlowfreq)
	med = np.median(dctlowfreq)
	# print(med)
	diff = dctlowfreq > med
	
	# print(diff)

	return dct

sample = Image.open("photo_2019-11-11_18-35-04.jpg")
sample2 = Image.open("pier_sea_surf_121897_1920x1080.jpg")
dct = DCT(sample)
dct2 = DCT(sample2)
# print(len(dct[0]))

_dwt = pywt.dwt(dct, 'haar')
_dwt2 = pywt.dwt(dct2, 'haar')
