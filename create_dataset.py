from PIL import Image, ImageFilter, ImageFont, ImageDraw, ImageEnhance
import os
from random import randint
import cv2

PATH = "/home/parsa/ProjectII/dataset_scrips/natural-images/selected"
# PATH = "/home/parsa/ProjectII/dataset_scrips/original_images"


def index_images(path):
    i = 0
    for files in os.listdir(path):
        file_name, file_format = os.path.splitext(files)
        # print(file_name)
        # print(file_format)
        dst = str(i) + file_format
        os.rename(path + "/" + files, path + "/" + dst)
        # print(path + "/" + files, path + "/" + dst)
        i += 1


"""
resize images
"""


def resize(path):
    os.mkdir(path+'/resize')
    for item in os.listdir(path):
        file, _format = os.path.splitext(item)
        if _format:
            # if os.path.isfile(PATH+item):
            im = Image.open(path + '/' + item)
            # imResize = im.resize(, randint(randint(0,1024), randint(0,1024))), Image.ANTIALIAS)
            imResize = im.resize((randint(1, 1024), randint(1, 1024)), Image.ANTIALIAS)
            # imResize = im.resize(())
            imResize.save(path + '/resize/' + file + '_resize' + _format, 'JPEG', quality=90)


"""
add filter to images
"""


def filter_image(path):
    dirs = os.listdir(path)
    for item in dirs:
        if os.path.isfile(path + item):
            im = Image.open(path + item)
            f, e = os.path.splitext(path + item)
            imFilter = im.filter(ImageFilter.BLUR)
            imFilter.save(f[:len(f)-1] + 'filter/'+f[-1]+ '.jpg', 'JPEG', quality=90)


"""
add text to Image
"""


def text(path, path_to_font):
    os.mkdir(path + '/text')
    for item in os.listdir(path):
        file, _format = os.path.splitext(item)
        if _format:
            im = Image.open(path + '/' + item)
            width, height = im.size
            draw = ImageDraw.Draw(im)
            font = ImageFont.truetype(path_to_font, randint(int(width/10), int(width/4)))
            draw.text((randint(0, int(width/2)), randint(0,int(height/4))), str(item), (randint(100,255),randint(100,255),randint(100,255)), font = font)
            # f, e = os.path.splitext(path + item)
            im.save(path + '/text/' + file + '_text'+_format, 'JPEG', quality=90)


"""
gaussian filter
"""


def gaussian(path):
    os.mkdir(path + '/gaussian')
    for items in os.listdir(path):
        file, _format = os.path.splitext(items)
        if _format:
            im = Image.open(path + '/' + items)
            im_filter = im.filter(ImageFilter.GaussianBlur(randint(2,30)))
            im_filter.save(path + '/gaussian/' + file + '_gaussian' + _format, 'JPEG', quality=90)

# """
# Laplacian filter
# """
# def laplacian():
#     # os.mkdir(PATH+'/Laplacian')
#     im = cv2.imread('/home/parsa/ProjectII/dataset_scrips/original_images/original/4.jpg')
#     im_laplacian = cv2.Laplacian(im,0)
#     cv2.imshow("image", im_laplacian)
#     cv2.waitKey(0)


"""
mean filter
"""


def mean(path):
    os.mkdir(PATH+'/mean')
    for items in os.listdir(path):
        file, _format = os.path.splitext(items)
        if _format:
            im = cv2.imread(path + '/' + items)
            figure_size = randint(2, 30)
            new_image = cv2.blur(im, (figure_size, figure_size))
            cv2.imwrite(path + '/mean/' + file + '_mean' + _format, new_image)


"""
diff between gaussian and mean
"""

"""
JPEG compression
"""


def jpeg(path):
    os.mkdir(path + '/JPEGCompression')
    for items in os.listdir(path):
        file, _format = os.path.splitext(items)
        if _format:
            im = Image.open(path + '/' + items)
            im.save(path + '/JPEGCompression/' + file + '_jpeg' + _format, 'JPEG', quaity = randint(10, 100))


"""
brightness
"""
def brightness(path):
    os.mkdir(path + '/brightness')
    for items in os.listdir(path):
        file, _format = os.path.splitext(items)
        if _format:
            im = Image.open(path + '/' + items)
            enhancer = ImageEnhance.Brightness(im)
            enhanced_im = enhancer.enhance(randint(5, 50)/10)
            enhanced_im.save(path + '/brightness/' + file + '_brightness' + _format, 'JPEG', quality = 90)


"""
contrast
"""


def contrast(PATH):
    os.mkdir(PATH+'/contrast')
    for items in os.listdir(PATH):
        file, _format = os.path.splitext(items)
        if _format:
            im = Image.open(PATH+ '/'+items)
            enhancer = ImageEnhance.Contrast(im)
            enhanced_im = enhancer.enhance(randint(3, 50)/10)
            enhanced_im = enhancer.enhance(randint(5, 50)/10)
            enhanced_im.save(PATH+'/contrast/'+file+'_contrast'+_format, 'JPEG', quality = 90)


"""
rotate
"""


def rotate(path):
    os.mkdir(path + '/rotate')
    for items in os.listdir(path):
        file, _format = os.path.splitext(items)
        if _format:
            im = Image.open(path + '/' + items)
            rotated = im.rotate(randint(0, 360))
            rotated.save(path + '/rotate/' + file + '_rotated' + _format, 'JPEG', quality=90)


text(PATH,'/home/parsa/ProjectII/dataset_scrips/fonts/Lemondrop Bold.ttf')
rotate(PATH)
contrast(PATH)
brightness(PATH)
gaussian(PATH)
mean(PATH)
resize(PATH)
jpeg(PATH)

